# Ruby on Rails sandbox project

```bash
git clean -xfd
```


OR

```bash
docker compose up
docker exec -it rails-postgresql-js-docker-rails-1 /bin/sh
```

```bash
cd app
gem install bundler
bundle config --local path 'vendor'
bundle install
export NODE_OPTIONS=--openssl-legacy-provider
export RAILS_ENV=test
bundle exec rails assets:precompile
bundle exec rails db:migrate
bundle exec rspec spec
```

For fun:
```bash
bundle exec rails server
```
